import {
    TASK_LIST_LOADED,
    CHANGE_SORT_TYPE,
    CHANGE_SORT_DIRECTION,
    CHANGE_PAGE,
    SET_TOKEN
} from '../actions'

const initialState = {
  taskList: [],
  sortType: undefined,
  sortDirection: undefined,
  page: undefined,
  token: undefined
}

const reducer = (state = initialState, action) => {

    switch(action.type) {
        case TASK_LIST_LOADED:
            return {
                ...state,
                taskList: action.payload.message
            }
        case CHANGE_PAGE:
            return {
                ...state,
                page: action.payload
            }

        case CHANGE_SORT_TYPE: 
            return {
                ...state,
                sortType: action.payload
            }

        case CHANGE_SORT_DIRECTION: 
            return {
                ...state,
                sortDirection: action.payload
            }

        case SET_TOKEN: 
            return {
                ...state,
                token: action.payload
            }

        default:
            return state
    }
};

export default reducer
