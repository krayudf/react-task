
export const TASK_LIST_LOADED = 'TASK_LIST_LOADED'
export const CHANGE_SORT_TYPE = 'CHANGE_SORT_TYPE'
export const CHANGE_SORT_DIRECTION = 'CHANGE_SORT_DIRECTION'
export const CHANGE_PAGE = 'CHANGE_PAGE'
export const SET_TOKEN = 'SET_TOKEN'


export function taskListLoaded(result) {
    return { type: TASK_LIST_LOADED, payload: result }
}

export function changePage(page) {
    return { type: CHANGE_PAGE, payload: page }
}

export function changeSortType(type) {
    return { type: CHANGE_SORT_TYPE, payload: type }
}

export function changeSortDirection(direction) {
    return { type: CHANGE_SORT_DIRECTION, payload: direction }
}

export function setToken(token) {
    return { type: SET_TOKEN, payload: token }
}
