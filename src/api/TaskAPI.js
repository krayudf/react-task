class TaskAPI{

    _BASE_API_URL = 'https://uxcandy.com/~shapoval/test-task-backend/v2/'
    _DEVELOPER = '?developer=krayushkin555'

    async getData(url = ''){
        const fullURL = this._BASE_API_URL + this._DEVELOPER + url
        const res = await fetch(fullURL, {headers:{'Accept':'application/json'}})
        if(!res.ok){
            throw new Error(`Не удалось получить данные ${fullURL}`)
        }
        const body = await res.json()
        return body
    }

    async addTask(task){
        const fullURL = this._BASE_API_URL + 'create' + this._DEVELOPER
        const res = await fetch(
            fullURL,
            {
                headers:{'Accept':'application/json'},
                method: 'POST',
                body: task
            })
        
        if(!res.ok){
            throw new Error(`Не удалось получить данные ${fullURL}`)
        }
        const body = await res.json()

        return body;
    }

    async updateTask(taskId, newText, status, token){
        const fullURL = this._BASE_API_URL + `edit/${taskId}` + this._DEVELOPER
        
        var data = new FormData()
        data.append("text", newText)
        data.append("token", token)
        data.append("status", status)
        
        const res = await fetch(
            fullURL, 
            {
                headers:{'Accept':'application/json'},
                method: 'POST',
                body: data
            })
        
        if(!res.ok){
            throw new Error(`Не обновить задачу ${fullURL}`)
        }
        const body = await res.json()
        return body;
    }

    async logIn(user){
        const fullURL = this._BASE_API_URL + 'login' + this._DEVELOPER
        const res = await fetch(
            fullURL, 
            {
                headers:{'Accept':'application/json'},
                method: 'POST',
                body: user
            })
        
        if(!res.ok){
            throw new Error(`Не удалось получить данные ${fullURL}`)
        }
        const body = await res.json()
        return body;
    }

    getTaskList({sortType, sortDirection, page }){
        let customURL = ''
        customURL += sortType ? `&sort_field=${sortType}` : ''
        customURL += sortDirection ? `&sort_direction=${sortDirection}` : ''
        customURL += page ? `&page=${page}` : ''
        return this.getData(customURL);
    }

}
export default  new TaskAPI();
