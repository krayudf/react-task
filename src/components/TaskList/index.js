import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Pagination } from 'antd';
import {
    taskListLoaded,
    changePage
} from '../../actions'
import TaskAPI from "../../api/TaskAPI"
import Task from '../Task'


class TaskList extends React.PureComponent {

    pageChangeHandler = (page) => {
        this.props.changePage(page)
    }

    componentDidMount = () => {
        TaskAPI.getTaskList({}).then((res) => {
            this.props.taskListLoaded(res)
        })
    }

    render() {
        const {
            taskList,
            token
        } = this.props

       if(!taskList || !taskList.tasks)
        return null

        return (
            <>
                {taskList.tasks.map( (task) => <Task token={token} key={task.id} taskInfo={task}/>)}
                <Pagination 
                    style={{marginTop: '15px'}}
                    onChange={this.pageChangeHandler}
                    defaultCurrent={1}
                    pageSize={3}
                    total={taskList.total_task_count*1}
                />
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        taskList: state.taskList,
        token: state.token
    }
}

function mapDispatchToProps (dispatch) {
    return {
        taskListLoaded: bindActionCreators(taskListLoaded, dispatch),
        changePage: bindActionCreators(changePage, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
