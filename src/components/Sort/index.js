import React from 'react'
import { Radio, Icon } from 'antd';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TaskAPI from "../../api/TaskAPI"

import {
    changeSortType,
    changeSortDirection,
    taskListLoaded,
} from '../../actions'

class Sort extends React.PureComponent {

    handleSortType = e => {
        const sortType = e.target.value
        this.props.changeSortType(sortType)
    }

    handleSortDirection = e => {
        const sortDirection = e.target.value
        this.props.changeSortDirection(sortDirection)
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.sortType !== this.props.sortType 
            || prevProps.sortDirection !== this.props.sortDirection
            || prevProps.page !== this.props.page ) {
                TaskAPI.getTaskList(this.props.state).then((res) => {
                    this.props.taskListLoaded(res)
                })
        }
    }

    render() {
        return (
            <div>
                <Radio.Group onChange={this.handleSortType}>
                    <Radio.Button value="username">Имя</Radio.Button>
                    <Radio.Button value="email">Email</Radio.Button>
                    <Radio.Button value="status">Статус</Radio.Button>
                </Radio.Group>
                &nbsp;
                <Radio.Group onChange={this.handleSortDirection}>
                    <Radio.Button value="asc"><Icon type="caret-up" /></Radio.Button>
                    <Radio.Button value="desc"><Icon type="caret-down" /></Radio.Button>
                </Radio.Group>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        state,
        sortType: state.sortType,
        sortDirection: state.sortDirection,
        page: state.page,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        changeSortType: bindActionCreators(changeSortType, dispatch),
        changeSortDirection: bindActionCreators(changeSortDirection, dispatch),
        taskListLoaded: bindActionCreators(taskListLoaded, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sort)
