import React from 'react'
import { Modal, Button, Input, notification } from 'antd'
import TaskAPI from "../../api/TaskAPI"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    setToken
} from '../../actions'

class Login extends React.PureComponent {
    state = {
        visible: false,
        loading: false,
        login: undefined,
        password: undefined,
    }

    showModal = () => {
        this.setState({
          visible: true,
        })
    }

    handleCancel = e => {
        this.setState({
            visible: false,
        })
    }

    changeLogin = e => {
        this.setState({
            login: e.target.value,
        })
    }

    changePassword = e => {
        this.setState({
            password: e.target.value,
        })
    }

    logOut = e => {
        localStorage.removeItem('test-task-token');
        this.props.setToken(undefined)

        notification['success']({
            message: 'Вы вышли из аккаунта'
            })
    }

    sendForm = () => {

        this.setState({
            loading: true,
        })

        var form = new FormData()
        form.append("username", this.state.login)
        form.append("password", this.state.password)

        TaskAPI.logIn(form).then((res) => {
            if(res.status === 'error') {
                notification['error']({
                message: 'Ошибка авторизации',
                description:
                    'Проверьте корректность формы',
                })
            }
            else {
                notification['success']({
                    message: 'Вы авторизованы'
                    })

                this.setState({
                    visible: false,
                })

                const token = res.message.token
                localStorage.setItem('test-task-token', token)
                this.props.setToken(token)
            }

            this.setState({
                loading: false,
            })
        })
    }

    render() {
        const { 
            login,
            password,
            loading
        } = this.state
        
        return (
            <>
                { !this.props.token ? 
                    <Button type="primary" shape="circle" icon="user" onClick={this.showModal}/> 
                    :
                    <Button type="danger" shape="circle" icon="logout" onClick={this.logOut}/>
                }
              <Modal
                    title="Авторизация"
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    >
                    <div style={{ marginBottom: 16 }}>
                        <Input onChange={this.changeLogin} addonBefore="Логин" placeholder="Логин" />
                    </div>

                    <div style={{ marginBottom: 16 }}>
                        <Input onChange={this.changePassword} type="password" addonBefore="Пароль" placeholder="Пароль" />
                    </div>

                    <Button 
                        onClick={this.sendForm} 
                        type="primary" block
                        disabled={!login || !password}
                        loading={loading}
                    >
                        Войти
                    </Button>
                </Modal>
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token
    }
}

function mapDispatchToProps (dispatch) {
    return {
        setToken: bindActionCreators(setToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
