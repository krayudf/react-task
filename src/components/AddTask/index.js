import React from 'react'
import { Modal, Button, Input, notification } from 'antd'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
    taskListLoaded,
} from '../../actions'
import TaskAPI from "../../api/TaskAPI"

class AddTask extends React.PureComponent {
    state = {
        visible: false,
        userName: undefined,
        email: undefined,
        text: undefined
    }

    showModal = () => {
        this.setState({
          visible: true,
        })
    }
    
    handleCancel = e => {
        this.setState({
            visible: false,
        })
    }

    changeNameField = e => {
        this.setState({
          userName: e.target.value,
        })
    }

    changeEmail = e => {
        this.setState({
            email: e.target.value,
        })
    }

    changeText = e => {
        this.setState({
            text: e.target.value,
        })
    }
    

    sendForm = () => {

        var form = new FormData()
        form.append("username", this.state.userName)
        form.append("email", this.state.email)
        form.append("text", this.state.text)

        TaskAPI.addTask(form).then((res) => {
            if(res.status === 'error') {
                notification['error']({
                message: 'Ошибка создания новой задачи',
                description:
                    'Все поля обязательны для заполнения. Првоерьте корректность Email.',
                })
            }
            else {
                notification['success']({
                    message: 'Задача добавлена'
                    })

                this.setState({
                    visible: false,
                  })

                TaskAPI.getTaskList(this.state).then((res) => {
                    this.props.taskListLoaded(res)
                })
            }
        })
    }

    render() {
        const { TextArea } = Input;
        const { 
            userName,
            email,
            text
        } = this.state

        return (
            <>
                <Button type="primary" onClick={this.showModal}>
                    Добавить задачу
                </Button>
                
                <Modal
                    title="Новая задача"
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    >
                    <div style={{ marginBottom: 16 }}>
                        <Input onChange={this.changeNameField} addonBefore="Имя" placeholder="Имя" />
                    </div>

                    <div style={{ marginBottom: 16 }}>
                        <Input onChange={this.changeEmail} addonBefore="Email" placeholder="Email" />
                    </div>
                    <div onChange={this.changeText} style={{ marginBottom: 16 }}>
                        <TextArea rows={4} />
                    </div>

                    <Button 
                        onClick={this.sendForm} 
                        type="primary" block
                        disabled={!userName || !email || !text}
                    >
                        Добавить задачу
                    </Button>
                </Modal>
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        state
    }
}

function mapDispatchToProps (dispatch) {
    return {
        taskListLoaded: bindActionCreators(taskListLoaded, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTask)
