import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TaskList from '../TaskList'
import Sort from '../Sort'
import AddTask from '../AddTask'
import Login from '../Login'
import './app.css'
import 'antd/dist/antd.css'
import {
  setToken
} from '../../actions'

class App extends React.PureComponent {

  componentDidMount = () => {
    const token = localStorage.getItem('test-task-token')
    if(token){
      this.props.setToken(token)
    }
  }

render() {
    return (
      <>
          <div className='app-container'>
          <div className='menu'>
            <Sort/>
            <AddTask/>
            <Login/>
          </div>
            <TaskList/>
          </div>
      </>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return {
      setToken: bindActionCreators(setToken, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(App)
