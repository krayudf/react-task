import React from 'react'
import { Icon, Button, Input, notification, Switch } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TaskAPI from "../../api/TaskAPI"
import {
    taskListLoaded,
} from '../../actions'
import './task.css'


class TaskEditForm extends React.PureComponent {
    constructor(props) {
        super(props)
        const {
            taskInfo
        } = this.props

        this.state = {
            newTaskText: taskInfo.text,
            status: taskInfo.status,
        }
    }

    textChangeHandler = e => {
        this.setState({
            newTaskText: e.target.value,
        })
    }

    statusChangeHandler = checked => {
        this.setState({
            status: checked ? 10 : 0,
        })
    }
    save = () => {
        const {
            taskInfo,
            token,
            state,
        } = this.props

        TaskAPI.updateTask(taskInfo.id, this.state.newTaskText, this.state.status, token).then((res) => {

            if(res.status === 'ok'){
                this.props.close()
                
                TaskAPI.getTaskList(state).then((res) => {
                    this.props.taskListLoaded(res)
                })

                notification['success']({
                    message: 'Задача обновлена'
                    })
            }
            else {
                notification['error']({
                    message: res.message.token
                })
            }
        })
    }

    render() {
        const {
            visible,
            taskInfo
        } = this.props
        if (!visible) return null
        const { TextArea } = Input;
        return (
            <>
            <TextArea onChange={this.textChangeHandler} rows={4} defaultValue={taskInfo.text} />
            <Switch
                style={{marginTop: "10px"}}
                checkedChildren={<Icon type="check" />}
                unCheckedChildren={<Icon type="close" />}
                defaultChecked={taskInfo.status === 10 ? true : false}
                onChange={this.statusChangeHandler}
            />
            <div style={{marginTop: "10px"}}>
                
                <Button onClick={this.props.close}>Отменить</Button>
                <Button onClick={this.save} style={{marginLeft: "10px"}} type="primary">Сохранить</Button>
            </div>
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        state
    }
}

function mapDispatchToProps (dispatch) {
    return {
        taskListLoaded: bindActionCreators(taskListLoaded, dispatch)
    }
}

const TaskEditFormConnected = connect(mapStateToProps, mapDispatchToProps)(TaskEditForm)

class Task extends React.PureComponent {

    state = {
        editMode: false,
    }

    showTextArea = () => {
        this.setState({
            editMode: true,
        })
    }

    hideTextArea = () => {
        this.setState({
            editMode: false,
        })
    }

    render() {
        const {
            taskInfo,
            token
        } = this.props

        const { editMode } = this.state
        

        return (
            <div className='task'>
                {token && !editMode ? <Button className="task__edit-button" type="dashed" onClick={this.showTextArea} icon="edit" /> : null}
               <div className='task__author'>Автор: {taskInfo.username} ({taskInfo.email}) {taskInfo.status === 10 ? <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" /> : null}</div>
                { !editMode ? <div className='task__text'>{taskInfo.text}</div> : null }
                
                <TaskEditFormConnected 
                    visible={editMode}
                    taskInfo={taskInfo}
                    close={this.hideTextArea}
                    save={this.save}
                    token={token}
                />
               
            </div>
        )
    }
}

export default Task
